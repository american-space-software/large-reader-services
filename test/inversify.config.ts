import "core-js/stable"
import "regenerator-runtime/runtime"
import "reflect-metadata"

import { Container } from "inversify"

import { AuthorRepository } from "../src/repository/author-repository"
import { AuthorRepositoryNodeImpl } from "../src/repository/node/author-repository-impl"

import { ChannelRepository } from "../src/repository/channel-repository"
import { ChannelRepositoryNodeImpl } from "../src/repository/node/channel-repository-impl"

import { ItemRepository } from "../src/repository/item-repository"
import { ItemRepositoryNodeImpl } from "../src/repository/node/item-repository-impl"

import { WalletService } from "../src/service/core/wallet-service"

import { AuthorService } from "../src/service/author-service"
import { ChannelService } from "../src/service/channel-service"
import { DatabaseService } from "../src/service/core/database-service"
import { PagingService } from "../src/service/core/paging-service"


import { ItemService } from "../src/service/item-service"
import { AuthorWebService } from "../src/service/web/author-web-service"
import { ChannelWebService } from "../src/service/web/channel-web-service"
import { ItemWebService } from "../src/service/web/item-web-service"

import { providers } from "ethers"



import { UiService } from "../src/service/core/ui-service"

import { TokenService } from "../src/service/token-service"
import { MetadataRepository } from "../src/repository/metadata-repository"
import { MetadataRepositoryNodeImpl } from "../src/repository/node/metadata-repository-impl"

import { HardhatWalletServiceImpl } from "./util/hardhat-wallet-service"
import { ImageGeneratorService } from "../src/service/core/image-generator-service"
import { TransactionIndexerService } from "../src/service/core/transaction-indexer-service"


import { ContractStateRepository } from "../src/repository/contract-state-repository";
import { ContractStateRepositoryBrowserImpl } from "../src/repository/browser/contract-state-repository-impl";
import { ERCEventRepositoryBrowserImpl } from "../src/repository/browser/erc-event-repository-impl";
import { ERCEventRepository } from "../src/repository/erc-event-repository"

import PouchDB from 'pouchdb-node';
import { SchemaService } from "../src/service/core/schema-service"
import { ContractStateService } from "../src/service/contract-state-service"
import { ERCEventService } from "../src/service/erc-event-service"

import { ImageRepository } from "../src/repository/image-repository"
import { ImageRepositoryNodeImpl } from "../src/repository/node/image-repository-impl"


import { AnimationRepository } from "../src/repository/animation-repository"
import { AnimationRepositoryNodeImpl } from "../src/repository/node/animation-repository-impl"


import { StaticPageRepository } from "../src/repository/static-page-repository"
import { StaticPageRepositoryNodeImpl } from "../src/repository/node/static-page-repository-impl"


import { AttributeTotalRepository } from "../src/repository/attribute-total-repository"
import { AttributeTotalRepositoryNodeImpl } from "../src/repository/node/attribute-total-repository-impl"


import { ReaderSettingsRepository } from "../src/repository/reader-settings-repository"
import { ReaderSettingsRepositoryBrowserImpl } from "../src/repository/browser/reader-settings-repository-impl"

import fs from "fs"
import { ComponentStateService } from "../src/service/core/component-state-service"
import { ComponentStateRepositoryBrowserImpl } from "../src/repository/browser/component-state-repository-impl"
import { ComponentStateRepository } from "../src/repository/component-state-repository"

let container: Container

async function getMainContainer() {

  if (container) return container

  container = new Container()

  function provider() {

    //@ts-ignore
    return new providers.Web3Provider(web3.currentProvider)  
  } 

 

  function contracts() {

    const contract = require('./util/contract.json')
    
    if (!contract.contractAddress) return []

    const c = require('./util/contract-abi.json')

    //Override address
    c['Channel'].address = contract.contractAddress

    return c
  }

  container.bind("contracts").toConstantValue(contracts())


  container.bind("framework7").toConstantValue({})
  container.bind("baseURI").toConstantValue("")
  container.bind("version").toConstantValue("test")
  container.bind("provider").toConstantValue(provider())
  container.bind("PouchDB").toConstantValue(PouchDB)

  container.bind<WalletService>("WalletService").to(HardhatWalletServiceImpl).inSingletonScope();

  container.bind<ChannelRepository>("ChannelRepository").to(ChannelRepositoryNodeImpl).inSingletonScope()
  container.bind<ItemRepository>("ItemRepository").to(ItemRepositoryNodeImpl).inSingletonScope()
  container.bind<AuthorRepository>("AuthorRepository").to(AuthorRepositoryNodeImpl).inSingletonScope()
  container.bind<MetadataRepository>("MetadataRepository").to(MetadataRepositoryNodeImpl).inSingletonScope()

  container.bind<ImageRepository>("ImageRepository").to(ImageRepositoryNodeImpl).inSingletonScope()
  container.bind<AnimationRepository>("AnimationRepository").to(AnimationRepositoryNodeImpl).inSingletonScope()
  container.bind<StaticPageRepository>("StaticPageRepository").to(StaticPageRepositoryNodeImpl).inSingletonScope()
  container.bind<AttributeTotalRepository>("AttributeTotalRepository").to(AttributeTotalRepositoryNodeImpl).inSingletonScope()

  container.bind<ReaderSettingsRepository>("ReaderSettingsRepository").to(ReaderSettingsRepositoryBrowserImpl).inSingletonScope()
  container.bind<ERCEventRepository>("ERCEventRepository").to(ERCEventRepositoryBrowserImpl).inSingletonScope()
  container.bind<ContractStateRepository>("ContractStateRepository").to(ContractStateRepositoryBrowserImpl).inSingletonScope()
  container.bind<ComponentStateRepository>("ComponentStateRepository").to(ComponentStateRepositoryBrowserImpl).inSingletonScope()



  container.bind(ChannelWebService).toSelf().inSingletonScope()
  container.bind(ItemWebService).toSelf().inSingletonScope()
  container.bind(AuthorWebService).toSelf().inSingletonScope()

  container.bind<DatabaseService>("DatabaseService").to(DatabaseService).inSingletonScope()
  container.bind(PagingService).toSelf().inSingletonScope()
  container.bind(TokenService).toSelf().inSingletonScope()
  container.bind(ImageGeneratorService).toSelf().inSingletonScope()
  container.bind(TransactionIndexerService).toSelf().inSingletonScope()


  container.bind<UiService>("UiService").to(UiService).inSingletonScope()
  container.bind<ItemService>("ItemService").to(ItemService).inSingletonScope()
  container.bind<ChannelService>("ChannelService").to(ChannelService).inSingletonScope()
  container.bind<AuthorService>("AuthorService").to(AuthorService).inSingletonScope()

  container.bind<SchemaService>("SchemaService").to(SchemaService).inSingletonScope()
  container.bind<ContractStateService>("ContractStateService").to(ContractStateService).inSingletonScope()
  container.bind<ERCEventService>("ERCEventService").to(ERCEventService).inSingletonScope()
  container.bind<ComponentStateService>("ComponentStateService").to(ComponentStateService).inSingletonScope()

  
  //Attach container to window so we can easily access it from the browser console
  globalThis.container = container


  await cleanup()

  return container
}

const cleanup = async () => {
  await fs.rmSync('./pouch', { recursive: true, force: true })
  await fs.rmSync('./test-repo', { recursive: true, force: true })
}


export {
  getMainContainer, container
}


