//@ts-nocheck


import { container, getMainContainer } from "./inversify.config"

import { TransactionIndexerService } from "../src/service/core/transaction-indexer-service"


import { WalletService } from "../src/service/core/wallet-service"
import { SchemaService } from "../src/service/core/schema-service"
import { ERCEventService } from "../src/service/erc-event-service"
import { ContractStateService } from "../src/service/contract-state-service"
import { ERCEventRepository } from "../src/repository/erc-event-repository"

const assert = require('assert')


let service:TransactionIndexerService
let walletService:WalletService
let schemaService:SchemaService

let contractStateService:ContractStateService
let ercEventService:ERCEventService
let ercEventRepository:ERCEventRepository

let user0
let user1
let user2
let user3
let user4

contract('TransactionIndexerService', (accounts) => {

  before(async () => {

    user0 = accounts[0]
    user1 = accounts[1]
    user2 = accounts[2]
    user3 = accounts[3]
    user4 = accounts[4]

    let container = await getMainContainer()

    service = container.get(TransactionIndexerService)
    walletService = container.get("WalletService")
    schemaService = container.get("SchemaService")
    ercEventService = container.get("ERCEventService")
    ercEventRepository = container.get("ERCEventRepository")

    contractStateService = container.get("ContractStateService")

    await walletService.initWallet()

    await schemaService.load(["contract-states", "erc-events"])

    let contract:Contract = await walletService.getContract("Channel")

    await service.init(contract)

    //After getting the real block number reset to this point in time so the tests pass. 
    service.blockNumber = 15740006


  })

  after(async () => {

  })

  it('should index erc-721 events', async () => {

    //Act
    await service.index()

    let result = await ercEventService.list(100, 0)

    //Assert
    assert.strictEqual(result.length, 95)

    //Check first
    assert.strictEqual(result[0].blockNumber, 15703766)
    assert.strictEqual(result[0].logIndex, 92)

    assert.strictEqual(result[94].blockNumber, 15226363)
    assert.strictEqual(result[94].logIndex, 59)

    // for (let e of result) {
    //   console.log(`${e.blockNumber} / ${e.logIndex}`)
    // }



    // let PouchDB = container.get("PouchDB")
    // await PouchDB.replicate(ercEventRepository.db, 'http://localhost:5984/erc-events', {live: true});


  })

  it('should get events by token ID', async () => {

    //Act
    let result = await ercEventService.getByTokenId(4, 100, 0)

    //Assert
    assert.strictEqual(result.length, 3)

    assert.strictEqual(result[0].blockNumber, 15703754)
    assert.strictEqual(result[0].logIndex, 188)

    assert.strictEqual(result[1].blockNumber, 15703754)
    assert.strictEqual(result[1].logIndex, 187)

    assert.strictEqual(result[2].blockNumber, 15226436)
    assert.strictEqual(result[2].logIndex, 468)

  })



})