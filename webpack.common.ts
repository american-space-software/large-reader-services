//@ts-nocheck
import "core-js/stable/index.js"
import "regenerator-runtime/runtime.js"
import "reflect-metadata"


import path, { dirname } from 'path'
import webpack from 'webpack'

import nodeExternals from 'webpack-node-externals'

import { createRequire } from 'module'
import { fileURLToPath } from 'url'

const require = createRequire(import.meta.url)

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

import CopyWebpackPlugin from 'copy-webpack-plugin'


const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default


import { CleanWebpackPlugin } from 'clean-webpack-plugin'

import packageConfig from './package.json' assert { type: "json" }

const VERSION = JSON.stringify(packageConfig.version)

let configs = []

const fileLoader = {
  loader: 'file-loader',
  options: {
    name: '[folder]/[name].[ext]'
  }
}

export default async () => {

  let nodeConfig = {
    entry: "./src/generate.ts",
    // target: "node",
    externalsPresets: { node: true },
    externals: {
      'convert-svg-to-png': 'convert-svg-to-png'
    },
    experiments: {
      outputModule: true
    },
    resolve: {
      extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
      extensionAlias: {
        ".js": [".js", ".ts"]
      }
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: '/node_modules/',
          loader: 'ts-loader'
        },
        {
          test: /\.ejs?$/,
          type: 'asset/source'
        }
      ]
    },  
    output: {
      filename: 'generate.js',
      libraryTarget: "module",
      library: {
        type: "module"
      },
      chunkFormat: 'module',
      path: path.resolve(__dirname, 'dist')
    },
    plugins: [
      new CleanWebpackPlugin({
        dangerouslyAllowCleanPatternsOutsideProject: true
      })
    ]
  }


  let browserConfig = {
    entry: "./src/index.ts",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: '/node_modules/',
          loader: 'ts-loader'
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        {
          test: /\.(png|jpe?g|gif|svg|eot)$/,
          use: [fileLoader]
        },
        {
          test: /\.(ttf|woff|woff2)$/,
          type: 'asset/inline'
        },
        {
          test: /\.f7.html$/,
          use: ['framework7-loader']
        }
      ]
    },
    resolve: {
      extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
      extensionAlias: {
        ".js": [".js", ".ts"]
      },
      alias: {
        buffer: 'buffer',
        process: 'process/browser.js'
      },
      fallback: {
        "path": require.resolve("path-browserify"),
        "util": require.resolve("util/"),
        "assert": require.resolve("assert/"),
        "stream": require.resolve("stream-browserify"),
        "crypto": require.resolve("crypto-browserify"),
        "fs": false
      }
    },
    plugins: [

      new CleanWebpackPlugin({
        dangerouslyAllowCleanPatternsOutsideProject: true
      }),
  
  
      new webpack.ProvidePlugin({
        process: 'process/browser.js',
      }),
  
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
  
      new webpack.ProvidePlugin({
        fetch: ['node-fetch', 'default'],
      }),
  
      new webpack.DefinePlugin({
        VERSION: VERSION
      }),
  
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css"
      }),
  
      new HTMLInlineCSSWebpackPlugin()
  
    ],
    output: {
      library: "reader",
      filename: '[name].reader.js',
      path: path.resolve(__dirname, 'dist')
    },
    optimization: {
      usedExports: true,
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all'
          },

        }
      }
    },

  }

  let swFilename = `sw-${VERSION.replace('"', '').replace('"', '')}.js`
  
  let serviceWorkerConfig = {
    entry: './src/sw.ts',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: '/node_modules/',
          loader: 'ts-loader',
        }
      ],
    },
    output: {
      filename: swFilename,
      path: path.resolve(__dirname, 'dist')
    },
    plugins: [
  
      new CleanWebpackPlugin({
        dangerouslyAllowCleanPatternsOutsideProject: true
      }),

      new webpack.DefinePlugin({
        VERSION: VERSION
      })
    ]
  }
  
  configs.push(serviceWorkerConfig)
  configs.push(nodeConfig)
  configs.push(browserConfig)


  return configs
}


